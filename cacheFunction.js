function cacheFunction(cb) {
    let cache = {};
    let counter = 0;

    return function () {
        //if cache is empty the store the arguments passed no need to check it then in cache.
        if (Object.keys(cache).length == 0) {
            counter++;
            let argumentsArray = [];
            for (let index = 0; index < arguments.length; index++) {
                argumentsArray.push(arguments[index]);

            }
            returnValueByCallBack = cb(argumentsArray);
            argumentsArray.push(returnValueByCallBack);
            cache[counter] = argumentsArray;
            return {
                value:returnValueByCallBack,
                message:"Arguments are diffrent"
            }
        }
        
        //if cache is not empty then else executes.
        else {
            //Check if the arguments passed to function is already passed before
            for (key in cache) {
                let flag = true;
                let previousCachedValues = cache[key];
                if (previousCachedValues.length - 1 == arguments.length) {
                    for (let index = 0; index < arguments.length; index++) {
                        if (typeof (arguments[index]) == typeof (previousCachedValues[index]))
                        {
                           if(JSON.stringify(arguments[index])==JSON.stringify(previousCachedValues[index]))
                            continue;
                            else{
                                flag=false;
                                break;
                            }
                        }    
                        else {
                            flag = false;
                            break;
                        }

                    }
                }

                if (flag == true) {
                    return {
                        value:previousCachedValues[arguments.length],
                        message:"Arguments are repeated"
                    }
                    break;
                }

            }

            //if arguments passed to function is new then store it in cache
            let argumentsArray = [];
            counter++;
            for (let index = 0; index < arguments.length; index++) {
                argumentsArray.push(arguments[index]);

            }
            returnValueByCallBack = cb(argumentsArray);
            argumentsArray.push(returnValueByCallBack);
            cache[counter] = argumentsArray;
            return {
                value:returnValueByCallBack,
                message:"Arguments are diffrent"
            }

        }

    }

}

module.exports = cacheFunction;