const counterFactory = require("../counterFactory.js");

let incrementDecrementObject = counterFactory();

//increment variable in closure scope
let incrementValue = incrementDecrementObject.increment();
let decrementValue = incrementDecrementObject.decrement();

if (incrementValue == 6 && decrementValue == 5) {
    console.log("Result:Passed");
    console.log("incremented and decremented value are", incrementValue, "and", decrementValue);
}
else
    console.log("your function failed the test");