const cacheFunction = require("../cacheFunction.js")

function sumFunction(parameterArray) //callback function which sum the elements of array passed to it if array contains only numbers otherwise return null
{

    let sum = 0;
    for (let index = 0; index < parameterArray.length; index++) {
        if (typeof(parameterArray[index])!="number")
            return null;
        else
            sum += parameterArray[index];
    }

    return sum;
}

let returnFunction = cacheFunction(sumFunction);
let answerObject=returnFunction(10, 20, 30, 6.7);
if(answerObject.value==66.7)
{
    let answerObject1=returnFunction(10, 20, 30, 6.7);
    let answerObject2=returnFunction(10, 20, 9.8, 60);
    
    if(answerObject1.message=="Arguments are repeated" && answerObject2.message=="Arguments are diffrent" && answerObject2.value==99.8 && answerObject1.value==66.7)
    console.log("Result:Passed Your function passed the test");
    else
    console.log("Your cache function failed the test");

}




