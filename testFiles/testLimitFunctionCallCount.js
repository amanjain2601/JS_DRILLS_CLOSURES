const functionInvoker = require("../limitFunctionCallCount.js");
let callBackReturnedValue=[];

function printCounter() { //callback Function
    return "hello";
}

let callBackInvoker = functionInvoker(printCounter, 5);

for (let callCount = 1; callCount <= 5; callCount++) {
    let value = callBackInvoker();
    if (value != null)
        callBackReturnedValue.push(value);
}


if (callBackInvoker()==null) {
    console.log("Result:Passed");
    console.log("Your callback function returned values",callBackReturnedValue);
}
else
    console.log("Your function failed the test");



