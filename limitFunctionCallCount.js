function limitFunctionCallCount(cb, n) {
    let counter = 0;
    return function () {
        counter++;
        if (counter <= n)
            return cb();
        else if (counter > n)
            return null;
    }
}


module.exports = limitFunctionCallCount;