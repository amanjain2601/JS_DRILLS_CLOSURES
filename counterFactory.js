function counterFactory() {
    let counter = 5;
    return {
        increment: function () {
            counter++;
            return counter;
        },

        decrement: function () {
            counter--;
            return counter;
        }
    }
}

module.exports = counterFactory;